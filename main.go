package main

import (
	"context"
	"io"
	"log"
	"net/http"
	"time"
)

// main includes an HTTP(s) client, to ensure that crypto libraries are linked in
// the binary. This is required to validate that the artifact uses the appropriate
// crypto mechanism when compiled in FIPS_MODE=1.
func main() {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, "https://www.gitlab.com", nil)
	if err != nil {
		log.Fatalf("%v", err)
	}

	client := http.DefaultClient

	res, err := client.Do(req)
	if err != nil {
		log.Fatalf("%v", err)
	}

	defer res.Body.Close()

	_, err = io.ReadAll(res.Body)
	if err != nil {
		log.Fatalf("Error %s", err)
	}
}
