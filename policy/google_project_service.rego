package main

import rego.v1

deny_google_project_service_disable_on_destroy contains msg if {
	some name
	service := input.resource.google_project_service[name]
	service.disable_on_destroy
	msg = sprintf("disable_on_destroy should be disabled for google_project_service `%v`", [name])
}
