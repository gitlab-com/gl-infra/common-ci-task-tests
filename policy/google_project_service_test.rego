package main

import rego.v1

test_deny_google_project_service_disable_on_destroy_fail if {
	code := {"resource": {"google_project_service": {"foo": {"disable_on_destroy": true}}}}
	deny_google_project_service_disable_on_destroy["disable_on_destroy should be disabled for google_project_service `foo`"] with input as code
}

test_deny_google_project_service_disable_on_destroy_pass if {
	code := {"resource": {"google_project_service": {"foo": {"disable_on_destroy": false}}}}
	count(deny_google_project_service_disable_on_destroy) == 0 with input as code
}
