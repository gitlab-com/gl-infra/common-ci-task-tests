FROM alpine:3.21.3

# Easy way of telling if caching is working
RUN echo "This is not cached" && sleep 10
