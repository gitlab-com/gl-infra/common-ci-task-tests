# common-ci-task-tests

This project is a set of tests for <https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/>.

It is used for downstream validation, and has no intrinsic utility of it's own.

## Hacking on common-ci-task-tests

### Preparing your Environment

1. Follow the developer setup guide at <https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/docs/developer-setup.md>.
1. Clone this project
1. Run `scripts/prepare-dev-env.sh`
